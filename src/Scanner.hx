class Scanner {
    final source:String;
    final tokens = new List<Token>();

    private var start = 0;
    private var current = 0;
    private var line = 1;

    static var keywords:Map<String,TokenType> = [
        "and" => And,
        "class" => Class,
        "else" => Else,
        "false" => False,
        "fun" => Fun,
        "for" => For,
        "if" => If,
        "nil" => Nil,
        "or" => Or,
        "print" => Print,
        "return" => Return,
        "super" => Super,
        "this" => This,
        "true" => True,
        "var" => Var,
        "while" => While
    ];

    public function new(source:String) {
        this.source = source;
    }

    public function scanTokens() {
        while (!atEnd()) {
            start = current;
            scanToken();
        }
        tokens.add(new Token(EOF, "", null, line));
        return tokens;
    }

    function scanToken() {
        var c = advance();
        switch (c) {
            case "(": addToken(LeftParen);
            case ")": addToken(RightParen);
            case "{": addToken(LeftBrace);
            case "}": addToken(RightBrace);
            case ",": addToken(Comma);
            case ".": addToken(Dot);
            case "+": addToken(Plus);
            case "-": addToken(Minus);
            case ";": addToken(Semicolon);
            case "*": addToken(Star);
            case "!": addToken(match("=") ? BangEq : Bang);
            case "=": addToken(match("=") ? DoubleEq : Eq);
            case "<": addToken(match("=") ? Lte : Lt);
            case ">": addToken(match("=") ? Gte : Gt);
            case "/": if (match("/")) {
                          while (peek() != "\n" && !atEnd()) advance();
                      } else {
                          addToken(Slash);
                      }
            case " " | "\r" | "\t": null;
            case "\n": line++;
            case '"': string();
            case n if (isDigit(n)): number();
            case a if (isAlpha(a)): identifier();
            case _: Lox.error(line, 'unexpected character "${c}"');
        }
    }

    function identifier() {
        while (isAlphaNumeric(peek())) advance();
        var text = source.substring(start, current);
        var type = keywords[text];

        if (type == null) type = Identifier;
        addToken(type);
    }

    function number() {
        while (isDigit(peek())) advance();
        if (peek() == "." && isDigit(peek(1))) {
            advance();
            while (isDigit(peek())) advance();
        }
        addToken(Number, Std.parseFloat(source.substring(start, current)));
    }

    function string() {
        while (peek() != '"' && !atEnd()) {
            if (peek() == "\n") line++;
            advance();
        }

        if (atEnd()) {
            Lox.error(line, "unterminated string");
            return;
        }

        advance();
        // TODO add escape sequences here
        addToken(String, source.substring(start + 1, current - 1));
    }

    function match(expected) {
        if (atEnd()) return false;
        if (source.charAt(current) != expected) return false;

        current++;
        return true;
    }

    function peek(add = 0) {
        return current+add >= source.length ? "\000" : source.charAt(current+add);
    }

    function advance() {
        return source.charAt(current++);
    }

    function isDigit(c) {
        var code = c.charCodeAt(0);
        return code >= "0".code && code <= "9".code;
    }

    function isAlpha(c) {
        var code = c.charCodeAt(0);
        return (code >= "a".code && code <= "z".code)
            || (code >= "A".code && code <= "Z".code)
            || code == "_".code;
    }

    function isAlphaNumeric(c) {
        return isAlpha(c) || isDigit(c);
    }

    function atEnd() {
        return current >= source.length;
    }

    function addToken(type:TokenType, literal = null) {
        var text = source.substring(start, current);
        tokens.add(new Token(type, text, literal, line));
    }
}

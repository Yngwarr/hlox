class Token {
    final type:TokenType;
    final lexeme:String;
    final literal:Any;
    final line:Int;

    public function new(type, lexeme, literal, line) {
        this.type = type;
        this.lexeme = lexeme;
        this.literal = literal;
        this.line = line;
    }

    public function toString() {
        return '${type} ${lexeme} ${literal}';
    }
}

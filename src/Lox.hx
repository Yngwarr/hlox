class Lox {
    static var hadError = false;

    static public function main() {
        var args = Sys.args();
        switch (args.length) {
            case 0: runPrompt();
            case 1: runFile(args[0]);
            case _: {
                Sys.stderr().writeString("Usage: hlox [source]\n");
                Sys.exit(64);
            };
        }
    }

    static function runFile(path:String) {
        var prog:String = sys.io.File.getContent(path);
        run(prog);
        if (hadError) Sys.exit(65);
    }

    static function runPrompt() {
        try {
            while (true) {
                Sys.print("> ");
                run(Sys.stdin().readLine());
                hadError = false;
            }
        } catch (e:haxe.io.Eof) {
            return;
        }
    }

    static function run(source) {
        var scanner = new Scanner(source);
        // List<Token>
        var tokens = scanner.scanTokens();

        Sys.println(tokens.join("\n"));
    }

    static public function error(line:Int, message:String) {
        report(line, "", message);
    }

    static function report(line:Int, where:String, message:String) {
        Sys.stderr().writeString('[line $line] Error${where}: ${message}\n');
        hadError = true;
    }
}

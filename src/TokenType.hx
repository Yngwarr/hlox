enum TokenType {
    // single-character tokens
    LeftParen; RightParen; LeftBrace; RightBrace;
    Comma; Dot; Minus; Plus; Semicolon; Slash; Star;

    // one or two character tokens
    Bang; BangEq;
    Eq; DoubleEq;
    Gt; Gte;
    Lt; Lte;

    // literals
    Identifier; String; Number;

    // keywords
    And; Class; Else; False; Fun; For; If; Nil; Or;
    Print; Return; Super; This; True; Var; While;

    EOF;
}

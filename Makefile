.PHONY: build run

build:
	haxe -p src --main Lox --hl lox.hl
run: build
	hl lox.hl
clean:
	rm lox.hl
